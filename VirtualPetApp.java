import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Dog[] groupOfDogs = new Dog[4];
		
		for(int i = 0; i < groupOfDogs.length; i++){
			System.out.println("\nInsert your dog's information");
			
			System.out.println("Enter your dog's name");
			String dogName = reader.nextLine();
			
			System.out.println("Enter your dog's age");
			int dogAge = Integer.parseInt(reader.nextLine());
			
			System.out.println("Enter the trick your dog can do :D");
			String dogTrick = reader.nextLine();
			
			groupOfDogs[i] = new Dog(dogName, dogAge, dogTrick);
		}
		
		//printing dog's attributes before the name change
		System.out.println("\nLast dog's information:\nDog name: " + groupOfDogs[groupOfDogs.length - 1].getName());
		System.out.println("Dog age: " + groupOfDogs[groupOfDogs.length - 1].getAge());
		System.out.println("Dog trick: " + groupOfDogs[groupOfDogs.length - 1].getTrick());
		
		//changing dog's name
		System.out.println("\nYour dog, " + groupOfDogs[groupOfDogs.length - 1].getName() + ", doesn't like its name :(\nCan you choose another one?");
		String newName = reader.nextLine();
		groupOfDogs[groupOfDogs.length - 1].setName(newName);
		
		//printing dog's attributes after the name change
		System.out.println("Dog name: " + groupOfDogs[groupOfDogs.length - 1].getName());
		System.out.println("Dog age: " + groupOfDogs[groupOfDogs.length - 1].getAge());
		System.out.println("Dog trick: " + groupOfDogs[groupOfDogs.length - 1].getTrick());
	}
}