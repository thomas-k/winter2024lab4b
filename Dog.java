public class Dog{
	private String name;
	private int age;
	private String trick;
	
	public Dog(String name, int age, String trick){
		this.name = name;
		this.age = age;
		this.trick = trick;
	}
	
	//method that calculates the age of the dog from human years to dog years
	public void calculateAge(){
		int ageInDogYears = age * 7;
		System.out.println("\nYour dogs age is: " + ageInDogYears + "years old.");
	}
	
	// doggo does a trick !
	public String doTrick(){
		return "\n" + "***" + name + " performed " + trick + "  ^_^" + "***";
	}
	
	//ALL GETTERS
	public String getName(){
		return this.name;
	}
	public String getTrick(){
		return this.trick;
	}
	public int getAge(){
		return this.age;
	}
	//ALL SETTERS
	public void setName(String name){
		this.name = name;
	}
}